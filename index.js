//  Users Collection (Users.json)
{
    "userID" : "user001",
    "firstName" : "Herohito",
    "lastName" : "Acuna",
    "email" : "herohitoacuna@gmail.com",
    "isAdmin" : true,
    "mobileNumber" : "09297633186"
}

{
    "userID" : "user002",
    "firstName" : "CJ",
    "lastName" : "Gerodias",
    "email" : "cjgerodias@gmail.com",
    "isAdmin" : false,
    "mobileNumber" : "09356761004"
}

// Products (Products.json)
{
    "productID" : "product001",
    "name" : "slippers",
    "description" : "Comfortable slippers",
    "price" : 100,
    // initial stocks 20
    "stocks" : 15,
    "isActive" : true,
    "SKU" : "123154-EQW"
}

{
    "productID" : "product002",
    "name" : "tshirt",
    "description" : "Made of Cotton",
    "price" : 150,
    // initial stocks 10
    "stocks" : 8,
    "isActive" : true,
    "SKU" : "235464-EMQ"
}

// Order Products (OrderProducts.json)
{
    "orderID" : "order001",
    "productID" : "product001",
    "quantity" : 5,
    "price" : 100,
    // subTotal = quantity * price
    "subTotal" : 500,
}

{
    "orderID" : "order002",
    "productID" : "product002",
    "quantity" : 2,
    "price" : 150,
    // subTotal = quantity * price
    "subTotal" : 300,
}

// Order (Order.json)
{
    "userID" : "user002",
    "transactionDate" : "2022-06-10T15:00:00.00Z"
    "status" : "To Ship",
    "total" : 800
}

